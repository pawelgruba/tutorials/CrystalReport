﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(AspNetWithCrystalReport.Startup))]
namespace AspNetWithCrystalReport
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
