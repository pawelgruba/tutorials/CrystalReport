﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AspNetWithCrystalReport.Controllers
{
    public class HomeController : Controller
    {
        private DataSet _dataSet;

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult ExportSalesByProduct()
        {
            CrystalDecisions.CrystalReports.Engine.ReportDocument doc = new CrystalDecisions.CrystalReports.Engine.ReportDocument();
            doc.Load(Path.Combine(Server.MapPath("~\\App_Data\\SalesByProduct.rpt")));

            using (SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CrystalReportForm.Properties.Settings.CrystalTutorialDBConnectionString"].ConnectionString))
            {
                _dataSet = new DataSet();



                using (SqlDataAdapter adapter = new SqlDataAdapter(@"SELECT P.Name, id.NetSalesPrice,id.Amount
                    FROM DBO.InvoiceDetails ID 
                    INNER JOIN DBO.Product AS P ON P.Id = ID.ProductId;", conn))
                {

                    adapter.Fill(_dataSet);
                    //_dataSet.Tables[0].TableName = "SalesByProducts";
                }
            }
            doc.SetDataSource(_dataSet);
            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();

            Stream stream = doc.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);

            return File(stream, "application/pdf","Report.pdf");
        }
    }
}