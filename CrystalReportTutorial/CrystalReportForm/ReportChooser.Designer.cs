﻿namespace CrystalReportForm
{
    partial class ReportChooser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tlpMain = new System.Windows.Forms.TableLayoutPanel();
            this.btnReportLoad = new System.Windows.Forms.Button();
            this.flpFlow = new System.Windows.Forms.FlowLayoutPanel();
            this.tlpMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // tlpMain
            // 
            this.tlpMain.ColumnCount = 1;
            this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpMain.Controls.Add(this.btnReportLoad, 0, 1);
            this.tlpMain.Controls.Add(this.flpFlow, 0, 0);
            this.tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpMain.Location = new System.Drawing.Point(0, 0);
            this.tlpMain.Name = "tlpMain";
            this.tlpMain.RowCount = 2;
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 80.84291F));
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 19.15709F));
            this.tlpMain.Size = new System.Drawing.Size(284, 261);
            this.tlpMain.TabIndex = 0;
            // 
            // btnReportLoad
            // 
            this.btnReportLoad.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnReportLoad.Location = new System.Drawing.Point(3, 213);
            this.btnReportLoad.Name = "btnReportLoad";
            this.btnReportLoad.Size = new System.Drawing.Size(278, 45);
            this.btnReportLoad.TabIndex = 0;
            this.btnReportLoad.Text = "Load Report";
            this.btnReportLoad.UseVisualStyleBackColor = true;
            this.btnReportLoad.Click += new System.EventHandler(this.btnReportLoad_Click);
            // 
            // flpFlow
            // 
            this.flpFlow.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpFlow.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flpFlow.Location = new System.Drawing.Point(3, 3);
            this.flpFlow.Name = "flpFlow";
            this.flpFlow.Size = new System.Drawing.Size(278, 204);
            this.flpFlow.TabIndex = 1;
            // 
            // ReportChooser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.tlpMain);
            this.Name = "ReportChooser";
            this.Text = "ReportChooser";
            this.Load += new System.EventHandler(this.ReportChooser_Load);
            this.tlpMain.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tlpMain;
        private System.Windows.Forms.Button btnReportLoad;
        private System.Windows.Forms.FlowLayoutPanel flpFlow;
    }
}