﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CrystalReportForm
{
    public partial class ReportWindow : Form
    {
        private ReportClass _report;
        private System.Data.DataSet _dataSet;

        public ReportWindow()
        {
            InitializeComponent();
        }



        public ReportWindow(ReportClass report, System.Data.DataSet dataSet)
        {
            this._report = report;
            this._dataSet = dataSet;
            InitializeComponent();

        }

        /// <summary>
        /// Event method on report control load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void crViewerControl_Load(object sender, EventArgs e)
        {
            try
            {
                var dynDataSet = _report;

                var databases = _report.Database.Tables;

                DataSets.CrystalTutorialDBDataSet dataSet = new DataSets.CrystalTutorialDBDataSet();
                DataSets.CrystalTutorialDBDataSetTableAdapters.InvoiceHeaderTableAdapter invoiceHeaderTableAdapter = new DataSets.CrystalTutorialDBDataSetTableAdapters.InvoiceHeaderTableAdapter();

                invoiceHeaderTableAdapter.Fill(dataSet.InvoiceHeader);

                _report.SetDataSource(dataSet);

                crViewerControl.ReportSource = _report;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
