﻿namespace CrystalReportForm
{
    partial class ReportWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.crViewerControl = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.SuspendLayout();
            // 
            // crViewerControl
            // 
            this.crViewerControl.ActiveViewIndex = -1;
            this.crViewerControl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.crViewerControl.Cursor = System.Windows.Forms.Cursors.Default;
            this.crViewerControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.crViewerControl.Location = new System.Drawing.Point(0, 0);
            this.crViewerControl.Name = "crViewerControl";
            this.crViewerControl.Size = new System.Drawing.Size(697, 412);
            this.crViewerControl.TabIndex = 0;
            this.crViewerControl.Load += new System.EventHandler(this.crViewerControl_Load);
            // 
            // ReportWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(697, 412);
            this.Controls.Add(this.crViewerControl);
            this.Name = "ReportWindow";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private CrystalDecisions.Windows.Forms.CrystalReportViewer crViewerControl;
    }
}

