﻿using CrystalDecisions.CrystalReports.Engine;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

namespace CrystalReportForm
{
    public partial class ReportChooser : Form
    {
        private List<Type> _availableReports;
        private string _query = @"SELECT P.Name, id.NetSalesPrice,id.Amount FROM DBO.InvoiceDetails ID INNER JOIN DBO.Product AS P ON P.Id = ID.ProductId";
        private string _connString = System.Configuration.ConfigurationManager.ConnectionStrings["CrystalReportForm.Properties.Settings.CrystalTutorialDBConnectionString"].ConnectionString;
        private ReportFactory _factory = new ReportFactory();
        public ReportChooser()
        {
            InitializeComponent();
        }


        private void ReportChooser_Load(object sender, EventArgs e)
        {
            //replaced by reportfactory class and its properties
            //_availableReports = Assembly.GetExecutingAssembly().GetTypes().Where(w => w.FullName.StartsWith("CrystalReportForm.Reports") && !w.Name.StartsWith("Cached")).ToList();

            foreach (var item in _factory.AvailableReports)
            {
                RadioButton rb = new RadioButton()
                {
                    Name = item.Value.Name,
                    Text = item.Value.Name,
                    Width = this.Width
                };

                flpFlow.Controls.Add(rb);
            }
        }

        private void btnReportLoad_Click(object sender, EventArgs e)
        {
            foreach(var control in flpFlow.Controls)
            {
                if (control.GetType() == typeof(RadioButton)
                    && ((RadioButton)control).Checked)
                {

                    KeyValuePair<string, Type> selectedReportType = _factory.AvailableReports.Where(w => w.Value.Name == ((RadioButton)control).Name).First();


                    var report = Activator.CreateInstance(selectedReportType.Value) as ReportClass;


                    CrystalReportPresenter.ReportScreen rs = new CrystalReportPresenter.ReportScreen(_connString, selectedReportType.Key);
                    rs.CrystalReport = report;
                    rs.Show();
                }
            }
            

            
        }
    }
}
