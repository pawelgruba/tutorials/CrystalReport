--EXEC msdb.dbo.sp_delete_database_backuphistory @database_name = N'CrystalTutorialDB'
--GO
--USE [master]
--GO
--ALTER DATABASE [CrystalTutorialDB] SET  SINGLE_USER WITH ROLLBACK IMMEDIATE
--GO
--USE [master]
--GO
--/****** Object:  Database [CrystalTutorialDB]    Script Date: 2017-08-01 13:23:22 ******/
--DROP DATABASE [CrystalTutorialDB]
--GO


USE [master]
GO

/****** Object:  Database [CrystalTutorialDB]    Script Date: 2017-07-31 12:41:20 ******/
CREATE DATABASE [CrystalTutorialDB]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'CrystalTutorialDB', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.SQL2012ENT\MSSQL\DATA\CrystalTutorialDB.mdf' , SIZE = 4160KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'CrystalTutorialDB_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.SQL2012ENT\MSSQL\DATA\CrystalTutorialDB_log.ldf' , SIZE = 1040KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO

ALTER DATABASE [CrystalTutorialDB] SET COMPATIBILITY_LEVEL = 110
GO

IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [CrystalTutorialDB].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO

ALTER DATABASE [CrystalTutorialDB] SET ANSI_NULL_DEFAULT OFF 
GO

ALTER DATABASE [CrystalTutorialDB] SET ANSI_NULLS OFF 
GO

ALTER DATABASE [CrystalTutorialDB] SET ANSI_PADDING OFF 
GO

ALTER DATABASE [CrystalTutorialDB] SET ANSI_WARNINGS OFF 
GO

ALTER DATABASE [CrystalTutorialDB] SET ARITHABORT OFF 
GO

ALTER DATABASE [CrystalTutorialDB] SET AUTO_CLOSE OFF 
GO

ALTER DATABASE [CrystalTutorialDB] SET AUTO_CREATE_STATISTICS ON 
GO

ALTER DATABASE [CrystalTutorialDB] SET AUTO_SHRINK OFF 
GO

ALTER DATABASE [CrystalTutorialDB] SET AUTO_UPDATE_STATISTICS ON 
GO

ALTER DATABASE [CrystalTutorialDB] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO

ALTER DATABASE [CrystalTutorialDB] SET CURSOR_DEFAULT  GLOBAL 
GO

ALTER DATABASE [CrystalTutorialDB] SET CONCAT_NULL_YIELDS_NULL OFF 
GO

ALTER DATABASE [CrystalTutorialDB] SET NUMERIC_ROUNDABORT OFF 
GO

ALTER DATABASE [CrystalTutorialDB] SET QUOTED_IDENTIFIER OFF 
GO

ALTER DATABASE [CrystalTutorialDB] SET RECURSIVE_TRIGGERS OFF 
GO

ALTER DATABASE [CrystalTutorialDB] SET  ENABLE_BROKER 
GO

ALTER DATABASE [CrystalTutorialDB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO

ALTER DATABASE [CrystalTutorialDB] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO

ALTER DATABASE [CrystalTutorialDB] SET TRUSTWORTHY OFF 
GO

ALTER DATABASE [CrystalTutorialDB] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO

ALTER DATABASE [CrystalTutorialDB] SET PARAMETERIZATION SIMPLE 
GO

ALTER DATABASE [CrystalTutorialDB] SET READ_COMMITTED_SNAPSHOT OFF 
GO

ALTER DATABASE [CrystalTutorialDB] SET HONOR_BROKER_PRIORITY OFF 
GO

ALTER DATABASE [CrystalTutorialDB] SET RECOVERY FULL 
GO

ALTER DATABASE [CrystalTutorialDB] SET  MULTI_USER 
GO

ALTER DATABASE [CrystalTutorialDB] SET PAGE_VERIFY CHECKSUM  
GO

ALTER DATABASE [CrystalTutorialDB] SET DB_CHAINING OFF 
GO

ALTER DATABASE [CrystalTutorialDB] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO

ALTER DATABASE [CrystalTutorialDB] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO

ALTER DATABASE [CrystalTutorialDB] SET  READ_WRITE 
GO



USE [CrystalTutorialDB]
GO
/****** Object:  Table [dbo].[Customer]    Script Date: 2017-07-31 12:41:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customer](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](350) NOT NULL,
	[Active] [bit] NOT NULL,
 CONSTRAINT [PK_Customer] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Employee]    Script Date: 2017-07-31 12:41:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Employee](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [nvarchar](50) NOT NULL,
	[LastName] [nvarchar](50) NOT NULL,
	[BirthDate] [date] NOT NULL,
 CONSTRAINT [PK_Employee] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

/****** Object:  Table [dbo].[InvoiceHeader]    Script Date: 2017-07-31 12:41:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[InvoiceHeader](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[OrderDate] [datetime2] NOT NULL,
	[SalesDate] [datetime2] NOT NULL,
	[InvoiceNumber] [nvarchar](50) NOT NULL,
	[CustomerId] [int] NOT NULL,
	[NetSum] [decimal](18,2) NULL,
	[GrossSum] [decimal](18, 2) NULL,
	[TotalItems] [int] NULL,
 CONSTRAINT [PK_InvoiceHeader] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[InvoiceDetails]    Script Date: 2017-07-31 12:41:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[InvoiceDetails](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ProductId] [int] NOT NULL,
	[InvoiceId] [int] not null foreign key references InvoiceHeader(Id),
	[NetSalesPrice] [decimal](18, 2) NOT NULL,
	[GrossSalesPrice] [decimal](18, 2) NOT NULL,
	[Amount] [int] NOT NULL,
 CONSTRAINT [PK_InvoiceDetails] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Manufacturer]    Script Date: 2017-07-31 12:41:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Manufacturer](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Country] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Manufacturer] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Product]    Script Date: 2017-07-31 12:41:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Product](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[PurchasePrice] [decimal](18, 2) NOT NULL,
	[RegularPrice] [decimal](18, 2) NOT NULL,
	[TaxRate] [decimal](18, 2) NOT NULL,
 CONSTRAINT [PK_Product] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[InvoiceHeader] ADD  CONSTRAINT [DF_InvoiceHeader_OrderDate]  DEFAULT (getdate()) FOR [OrderDate]
GO
ALTER TABLE [dbo].[InvoiceHeader] ADD  CONSTRAINT [DF_InvoiceHeader_SalesDate]  DEFAULT (getdate()) FOR [SalesDate]
GO
ALTER TABLE [dbo].[InvoiceDetails]  WITH CHECK ADD  CONSTRAINT [FK_InvoiceDetails_Product] FOREIGN KEY([ProductId])
REFERENCES [dbo].[Product] ([Id])
GO
ALTER TABLE [dbo].[InvoiceDetails] CHECK CONSTRAINT [FK_InvoiceDetails_Product]
GO
ALTER TABLE [dbo].[InvoiceHeader]  WITH CHECK ADD  CONSTRAINT [FK_InvoiceHeader_Customer] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customer] ([Id])
GO
ALTER TABLE [dbo].[InvoiceHeader] CHECK CONSTRAINT [FK_InvoiceHeader_Customer]
GO
