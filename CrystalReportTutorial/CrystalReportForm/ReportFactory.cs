﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CrystalReportForm
{
    public class ReportFactory
    {
        public ReportFactory()
        {
            AvailableReports = new Dictionary<string, Type>()
            {
                {
                    @"SELECT P.Name, id.NetSalesPrice,id.Amount
                    FROM DBO.InvoiceDetails ID 
                    INNER JOIN DBO.Product AS P ON P.Id = ID.ProductId;",typeof(Reports.SalesByProduct)
                },
                {
                    @"SELECT ih.SalesDate, id.NetSalesPrice, id.Amount
                    FROM Product P
                    INNER JOIN InvoiceDetails id on id.ProductId = p.Id
                    inner join InvoiceHeader ih on ih.Id = id.InvoiceId", typeof(Reports.SalesByYearAndMonthUsingQuery)
                },
                {
                    @"SELECT ih.SalesDate as SalesDate, id.NetSalesPrice, id.Amount
                    FROM Product P
                    INNER JOIN InvoiceDetails id on id.ProductId = p.Id
                    inner join InvoiceHeader ih on ih.Id = id.InvoiceId",typeof(Reports.SalesByYearAndMonthUsingQueryAndNativeSQLClient)
                }
            };
        }
        public Dictionary<string, Type> AvailableReports;
    }
}
