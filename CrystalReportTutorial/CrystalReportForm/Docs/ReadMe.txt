﻿Typowe błędy:
An unhandled exception of type 'System.IO.FileNotFoundException' occurred in mscorlib.dll
======================== 1. ==================================
==Błąd:
Additional information: Could not load file or assembly 'file:///C:\Program Files (x86)\SAP BusinessObjects\Crystal Reports for .NET Framework 4.0\Common\SAP BusinessObjects Enterprise XI 4.0\win32_x86\dotnet1\crdb_adoplus.dll' or one of its dependencies.

==Rozwiązanie:
dodać w app.config klucz 'useLegacy....'
<startup useLegacyV2RuntimeActivationPolicy="true">
   <supportedRuntime version="v4.0" sku=".NETFramework,Version=v4.0"/>
</startup>

==Źródło:
https://stackoverflow.com/questions/27440663/exception-with-crystal-reports


===============================================
Tworzenie raportów:

1. Format daty jako string lub coś innego:
dzieje się tak gdy przy definiowaniu połaczenia nie wybierzemy ODBC->SQL NATIVE CLIENT 11 ale coś innego, w takim przypadku format dla dat jest przydzielany błędnie 
