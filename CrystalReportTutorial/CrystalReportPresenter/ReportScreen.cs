﻿using CrystalDecisions.CrystalReports.Engine;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CrystalReportPresenter
{
    public partial class ReportScreen : Form
    {
        private string _connectionString;
        private string _sqlQuery;
        private DataSet _dataSet;
        private ReportClass _report;

        public ReportScreen()
        {
            InitializeComponent();
        }

        public ReportClass CrystalReport
        {
            get
            {
                return _report;
            }
            set
            {
                _report = value;
            }
        }

        public ReportScreen(string connString, string query)
        {
            InitializeComponent();
            this._connectionString = connString;
            this._sqlQuery = query;
        }

        public new void Show()
        {
            using (SqlConnection conn = new SqlConnection(_connectionString))
            {
                _dataSet = new DataSet();



                using (SqlDataAdapter adapter = new SqlDataAdapter(_sqlQuery, conn))
                {

                    adapter.Fill(_dataSet);
                    //_dataSet.Tables[0].TableName = "SalesByProducts";
                }
            }
            _report.SetDataSource(_dataSet);
            //CrystalReportViewer.ReportSource = _report;
            base.Show();
        }

        private void CrystalReportViewer_Load(object sender, EventArgs e)
        {
            CrystalReportViewer.ReportSource = _report;
        }
    }
}
